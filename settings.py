import os

BASE_DIR = os.path.expanduser('~/.colorize_life')
os.makedirs(BASE_DIR, exist_ok=True)

CONFIG_FILE = os.path.expanduser(os.path.join(BASE_DIR, 'config.json'))

REG_FILE = os.path.join(BASE_DIR, 'user.json')

try:
    from local_settings import *
except ImportError:
    pass
