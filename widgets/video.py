from math import ceil

import cv2
import numpy as np
from PySide2 import QtGui
from PySide2.QtCore import QTimer
from PySide2.QtGui import QPixmap
from PySide2.QtWidgets import QLabel


class CFVideoWidget(QLabel):

    def __init__(self, parent):
        super().__init__(parent)
        self._parent = parent
        self.cap = None
        self.timer = QTimer()
        self.timer.timeout.connect(self._display_video_stream)
        self.timer.start(30)
        self._raw_frame = None
        self._frame = None
        self._stopped = False

    def _display_video_stream(self):
        if self._stopped:
            return
        frame = self.grab_video_frame()

        if frame is None:
            return

        self._frame = frame.copy()

        # Locate the video frame to the center
        h, w = frame.shape[:2]
        tw, th = self.width(), self.height()
        template = np.zeros((th, tw) if len(frame.shape) == 2 else (th, tw, 3), np.uint8)
        self.p_lt = ceil((tw - w) / 2), ceil((th - h) / 2)
        self.p_rb = tw - (tw - w) // 2, th - (th - h) // 2
        template[self.p_lt[1]:self.p_rb[1], self.p_lt[0]:self.p_rb[0]] = frame
        template = cv2.cvtColor(template, cv2.COLOR_BGR2RGB)
        img = QtGui.QImage(template, template.shape[1], template.shape[0], QtGui.QImage.Format_RGB888)
        self.setPixmap(QPixmap.fromImage(img))

    def get_frame(self):
        return self._frame

    def hideEvent(self, event: QtGui.QHideEvent):
        self.timer.stop()
        super(CFVideoWidget, self).hideEvent(event)

    def pause(self):
        self._stopped = True

    def resume(self):
        self._stopped = False
