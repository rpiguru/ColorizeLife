from functools import partial

from ui.ui_dlg_settings import Ui_SettingsDialog
from utils.common import get_config, update_config_file
from utils.logger import logger
from widgets.dialogs.base import DialogBase


class OptionDialog(DialogBase):

    def __init__(self, parent):
        super().__init__(parent)
        self.ui = Ui_SettingsDialog()
        self.ui.setupUi(self)
        self.ui.btnClose.released.connect(self.close)
        if get_config().get('save_format', 'jpg') == 'jpg':
            self.ui.radioJPEG.setChecked(True)
        else:
            self.ui.radioPNG.setChecked(True)
        self.ui.radioJPEG.toggled.connect(partial(self._on_changed, 'jpg'))
        self.ui.radioPNG.toggled.connect(partial(self._on_changed, 'png'))

    @staticmethod
    def _on_changed(save_format, *args):
        if args[0]:
            logger.info(f"New image format: {save_format}")
            update_config_file({"save_format": save_format})
