from ui.ui_dlg_register import Ui_RegisterDialog
from widgets.dialogs.base import DialogBase


class RegisterDialog(DialogBase):

    def __init__(self, parent):
        super().__init__(parent)
        self.ui = Ui_RegisterDialog()
        self.ui.setupUi(self)
        self.ui.btnClose.released.connect(self.close)
