from ui.ui_dlg_about import Ui_AboutDialog
from widgets.dialogs.base import DialogBase


class AboutDialog(DialogBase):

    def __init__(self, parent):
        super().__init__(parent)
        self.ui = Ui_AboutDialog()
        self.ui.setupUi(self)
        self.ui.btnClose.released.connect(self.close)
