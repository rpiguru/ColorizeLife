import os
import pathlib

from PIL import Image
from deoldify import device
from deoldify.device_id import DeviceId
from deoldify.visualize import get_artistic_image_colorizer, get_watermarked
import numpy as np
import cv2


_par_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))


class DeOldifier(object):

    def __init__(self, render_factor=25):
        device.set(device=DeviceId.CPU)
        self.vis = get_artistic_image_colorizer(root_folder=pathlib.Path(_par_dir), render_factor=render_factor)

    def process_image(self, path, render_factor=25, post_process=True, watermarked=True) -> Image:
        img = self.vis.get_transformed_image(
            path, render_factor, post_process=post_process, watermarked=watermarked
        )
        return img

    def process_frame(self, frame, render_factor=25, post_process=True):
        # Convert from numpy frame to PIL Image object
        img = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_GRAY2RGB if len(frame.shape) == 2 else cv2.COLOR_BGR2RGB))
        getattr(self.vis, "_clean_mem")()
        filtered_img = self.vis.filter.filter(img, img, render_factor=render_factor, post_process=post_process)
        return cv2.cvtColor(np.asarray(filtered_img), cv2.COLOR_RGB2BGR)


if __name__ == '__main__':

    d = DeOldifier()

    _frame = cv2.imread('test_images/ex1.jpg')

    colorized = d.process_frame(_frame)

    cv2.imshow("Colorized", colorized)
    cv2.waitKey(0)
