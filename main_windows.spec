# -*- mode: python ; coding: utf-8 -*-

block_cipher = None

a = Analysis(['main.py'],
             pathex=['E:\\workspace\\ColorizeLife'],
             binaries=[
                ('models/ColorizeArtistic_gen.pth', './models/'),
             ],
             datas=[
                ('colorize_life.ico', '.'),
                ('dll\\opencv_ffmpeg330_64.dll', '.'),
                ('dll\\api-ms-win-downlevel-shlwapi-l1-1-0.dll', '.'),
             ],
             hiddenimports=[
                'pandas._libs.tslibs.base',
             ],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='Colorize LIFE',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False,
          uac_admin=True,
          icon="colorize_life.ico"
          )

coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='colorize_life')
