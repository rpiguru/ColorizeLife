# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'dlg_about.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import ui.cf_rc

class Ui_AboutDialog(object):
    def setupUi(self, AboutDialog):
        if not AboutDialog.objectName():
            AboutDialog.setObjectName(u"AboutDialog")
        AboutDialog.resize(714, 494)
        AboutDialog.setStyleSheet(u"#AboutDialog {\n"
"background-image: url(:/img/img/Component 16 \u2013 1.png);\n"
"}")
        self.btnClose = QToolButton(AboutDialog)
        self.btnClose.setObjectName(u"btnClose")
        self.btnClose.setGeometry(QRect(640, 10, 53, 73))
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnClose.sizePolicy().hasHeightForWidth())
        self.btnClose.setSizePolicy(sizePolicy)
        self.btnClose.setStyleSheet(u"border: none;")
        icon = QIcon()
        icon.addFile(u":/img/img/x.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnClose.setIcon(icon)
        self.btnClose.setIconSize(QSize(50, 70))
        self.btnClose.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnClose.setAutoRaise(True)

        self.retranslateUi(AboutDialog)

        QMetaObject.connectSlotsByName(AboutDialog)
    # setupUi

    def retranslateUi(self, AboutDialog):
        AboutDialog.setWindowTitle(QCoreApplication.translate("AboutDialog", u"Dialog", None))
        self.btnClose.setText("")
    # retranslateUi

