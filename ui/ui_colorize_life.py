# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'colorize_life.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import ui.cf_rc

class Ui_ColorizeLife(object):
    def setupUi(self, ColorizeLife):
        if not ColorizeLife.objectName():
            ColorizeLife.setObjectName(u"ColorizeLife")
        ColorizeLife.resize(1476, 938)
        ColorizeLife.setStyleSheet(u"#ColorizeLife {\n"
"background-color: rgb(0, 0, 0);\n"
"}")
        self.horizontalLayout = QHBoxLayout(ColorizeLife)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.leftFrame = QFrame(ColorizeLife)
        self.leftFrame.setObjectName(u"leftFrame")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.leftFrame.sizePolicy().hasHeightForWidth())
        self.leftFrame.setSizePolicy(sizePolicy)
        self.leftFrame.setMinimumSize(QSize(90, 0))
        self.leftFrame.setMaximumSize(QSize(90, 16777215))
        self.leftFrame.setLayoutDirection(Qt.LeftToRight)
        self.leftFrame.setStyleSheet(u"#leftFrame{\n"
"	border-image: url(:/img/img/Rectangle 2.png);\n"
"}")
        self.leftFrame.setFrameShape(QFrame.NoFrame)
        self.leftFrame.setFrameShadow(QFrame.Plain)
        self.verticalLayout_2 = QVBoxLayout(self.leftFrame)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(9, 10, -1, -1)
        self.btnProcess = QToolButton(self.leftFrame)
        self.btnProcess.setObjectName(u"btnProcess")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.btnProcess.sizePolicy().hasHeightForWidth())
        self.btnProcess.setSizePolicy(sizePolicy1)
        self.btnProcess.setStyleSheet(u"border: none;")
        icon = QIcon()
        icon.addFile(u":/img/img/Group 142.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnProcess.setIcon(icon)
        self.btnProcess.setIconSize(QSize(70, 90))

        self.verticalLayout_2.addWidget(self.btnProcess)

        self.verticalSpacer = QSpacerItem(20, 20, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)

        self.btnClear = QToolButton(self.leftFrame)
        self.btnClear.setObjectName(u"btnClear")
        sizePolicy1.setHeightForWidth(self.btnClear.sizePolicy().hasHeightForWidth())
        self.btnClear.setSizePolicy(sizePolicy1)
        self.btnClear.setStyleSheet(u"border: none;")
        icon1 = QIcon()
        icon1.addFile(u":/img/img/clear.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnClear.setIcon(icon1)
        self.btnClear.setIconSize(QSize(50, 70))
        self.btnClear.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnClear.setAutoRaise(True)

        self.verticalLayout_2.addWidget(self.btnClear)

        self.btnAdd = QToolButton(self.leftFrame)
        self.btnAdd.setObjectName(u"btnAdd")
        sizePolicy1.setHeightForWidth(self.btnAdd.sizePolicy().hasHeightForWidth())
        self.btnAdd.setSizePolicy(sizePolicy1)
        self.btnAdd.setStyleSheet(u"border: none;")
        icon2 = QIcon()
        icon2.addFile(u":/img/img/Component 4 \u2013 1.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnAdd.setIcon(icon2)
        self.btnAdd.setIconSize(QSize(50, 70))
        self.btnAdd.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnAdd.setAutoRaise(True)

        self.verticalLayout_2.addWidget(self.btnAdd)

        self.btnMove = QToolButton(self.leftFrame)
        self.btnMove.setObjectName(u"btnMove")
        sizePolicy1.setHeightForWidth(self.btnMove.sizePolicy().hasHeightForWidth())
        self.btnMove.setSizePolicy(sizePolicy1)
        self.btnMove.setLayoutDirection(Qt.LeftToRight)
        self.btnMove.setStyleSheet(u"border: none;")
        icon3 = QIcon()
        icon3.addFile(u":/img/img/Component 5 \u2013 1.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnMove.setIcon(icon3)
        self.btnMove.setIconSize(QSize(50, 70))
        self.btnMove.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnMove.setAutoRaise(True)

        self.verticalLayout_2.addWidget(self.btnMove)

        self.btnRestore = QToolButton(self.leftFrame)
        self.btnRestore.setObjectName(u"btnRestore")
        sizePolicy1.setHeightForWidth(self.btnRestore.sizePolicy().hasHeightForWidth())
        self.btnRestore.setSizePolicy(sizePolicy1)
        self.btnRestore.setStyleSheet(u"border: none;")
        icon4 = QIcon()
        icon4.addFile(u":/img/img/Component 6 \u2013 1.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnRestore.setIcon(icon4)
        self.btnRestore.setIconSize(QSize(50, 70))
        self.btnRestore.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnRestore.setAutoRaise(True)

        self.verticalLayout_2.addWidget(self.btnRestore)

        self.btnCrop = QToolButton(self.leftFrame)
        self.btnCrop.setObjectName(u"btnCrop")
        sizePolicy1.setHeightForWidth(self.btnCrop.sizePolicy().hasHeightForWidth())
        self.btnCrop.setSizePolicy(sizePolicy1)
        self.btnCrop.setStyleSheet(u"border: none;")
        icon5 = QIcon()
        icon5.addFile(u":/img/img/Component 7 \u2013 1.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnCrop.setIcon(icon5)
        self.btnCrop.setIconSize(QSize(50, 70))
        self.btnCrop.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnCrop.setAutoRaise(True)

        self.verticalLayout_2.addWidget(self.btnCrop)

        self.btnColorize = QToolButton(self.leftFrame)
        self.btnColorize.setObjectName(u"btnColorize")
        sizePolicy1.setHeightForWidth(self.btnColorize.sizePolicy().hasHeightForWidth())
        self.btnColorize.setSizePolicy(sizePolicy1)
        self.btnColorize.setStyleSheet(u"border: none;")
        icon6 = QIcon()
        icon6.addFile(u":/img/img/Component 8 \u2013 1.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnColorize.setIcon(icon6)
        self.btnColorize.setIconSize(QSize(50, 70))
        self.btnColorize.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnColorize.setAutoRaise(True)

        self.verticalLayout_2.addWidget(self.btnColorize)

        self.btnImpaint = QToolButton(self.leftFrame)
        self.btnImpaint.setObjectName(u"btnImpaint")
        sizePolicy1.setHeightForWidth(self.btnImpaint.sizePolicy().hasHeightForWidth())
        self.btnImpaint.setSizePolicy(sizePolicy1)
        self.btnImpaint.setStyleSheet(u"border: none;")
        icon7 = QIcon()
        icon7.addFile(u":/img/img/Component 9 \u2013 1.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnImpaint.setIcon(icon7)
        self.btnImpaint.setIconSize(QSize(50, 70))
        self.btnImpaint.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnImpaint.setAutoRaise(True)

        self.verticalLayout_2.addWidget(self.btnImpaint)

        self.btnFitscreen = QToolButton(self.leftFrame)
        self.btnFitscreen.setObjectName(u"btnFitscreen")
        sizePolicy1.setHeightForWidth(self.btnFitscreen.sizePolicy().hasHeightForWidth())
        self.btnFitscreen.setSizePolicy(sizePolicy1)
        self.btnFitscreen.setStyleSheet(u"border: none;")
        icon8 = QIcon()
        icon8.addFile(u":/img/img/Component 10 \u2013 1.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnFitscreen.setIcon(icon8)
        self.btnFitscreen.setIconSize(QSize(50, 70))
        self.btnFitscreen.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnFitscreen.setAutoRaise(True)

        self.verticalLayout_2.addWidget(self.btnFitscreen)

        self.btnZoomin = QToolButton(self.leftFrame)
        self.btnZoomin.setObjectName(u"btnZoomin")
        sizePolicy1.setHeightForWidth(self.btnZoomin.sizePolicy().hasHeightForWidth())
        self.btnZoomin.setSizePolicy(sizePolicy1)
        self.btnZoomin.setStyleSheet(u"border: none;")
        icon9 = QIcon()
        icon9.addFile(u":/img/img/Component 11 \u2013 1.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnZoomin.setIcon(icon9)
        self.btnZoomin.setIconSize(QSize(50, 70))
        self.btnZoomin.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnZoomin.setAutoRaise(True)

        self.verticalLayout_2.addWidget(self.btnZoomin)

        self.btnZoomout = QToolButton(self.leftFrame)
        self.btnZoomout.setObjectName(u"btnZoomout")
        sizePolicy1.setHeightForWidth(self.btnZoomout.sizePolicy().hasHeightForWidth())
        self.btnZoomout.setSizePolicy(sizePolicy1)
        self.btnZoomout.setStyleSheet(u"border: none;")
        icon10 = QIcon()
        icon10.addFile(u":/img/img/Component 12 \u2013 1.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnZoomout.setIcon(icon10)
        self.btnZoomout.setIconSize(QSize(50, 70))
        self.btnZoomout.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnZoomout.setAutoRaise(True)

        self.verticalLayout_2.addWidget(self.btnZoomout)

        self.verticalSpacer_2 = QSpacerItem(20, 20, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer_2)


        self.horizontalLayout.addWidget(self.leftFrame)

        self.mainFrame = QFrame(ColorizeLife)
        self.mainFrame.setObjectName(u"mainFrame")
        self.mainFrame.setStyleSheet(u"#mainFrame {\n"
"  background-color: rgb(0, 0, 0);\n"
"}")
        self.mainFrame.setFrameShape(QFrame.StyledPanel)
        self.mainFrame.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(self.mainFrame)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.headerFrame = QFrame(self.mainFrame)
        self.headerFrame.setObjectName(u"headerFrame")
        sizePolicy2 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.headerFrame.sizePolicy().hasHeightForWidth())
        self.headerFrame.setSizePolicy(sizePolicy2)
        self.headerFrame.setMinimumSize(QSize(0, 80))
        self.headerFrame.setMaximumSize(QSize(16777215, 80))
        self.headerFrame.setStyleSheet(u"#headerFrame{\n"
"  background-color: rgb(17, 17, 17);\n"
"}")
        self.headerFrame.setFrameShape(QFrame.StyledPanel)
        self.headerFrame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_3 = QHBoxLayout(self.headerFrame)
        self.horizontalLayout_3.setSpacing(30)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(20, -1, 20, -1)
        self.horizontalSpacer = QSpacerItem(433, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer)

        self.label = QLabel(self.headerFrame)
        self.label.setObjectName(u"label")
        self.label.setPixmap(QPixmap(u":/img/img/icon-and-logo-upwork.png"))

        self.horizontalLayout_3.addWidget(self.label)

        self.horizontalSpacer_2 = QSpacerItem(434, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_2)

        self.btnOptions = QToolButton(self.headerFrame)
        self.btnOptions.setObjectName(u"btnOptions")
        sizePolicy1.setHeightForWidth(self.btnOptions.sizePolicy().hasHeightForWidth())
        self.btnOptions.setSizePolicy(sizePolicy1)
        self.btnOptions.setStyleSheet(u"border: none;")
        icon11 = QIcon()
        icon11.addFile(u":/img/img/Group 60.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnOptions.setIcon(icon11)
        self.btnOptions.setIconSize(QSize(50, 70))
        self.btnOptions.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnOptions.setAutoRaise(True)

        self.horizontalLayout_3.addWidget(self.btnOptions)

        self.btnAbout = QToolButton(self.headerFrame)
        self.btnAbout.setObjectName(u"btnAbout")
        sizePolicy1.setHeightForWidth(self.btnAbout.sizePolicy().hasHeightForWidth())
        self.btnAbout.setSizePolicy(sizePolicy1)
        self.btnAbout.setStyleSheet(u"border: none;")
        icon12 = QIcon()
        icon12.addFile(u":/img/img/Group 61.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnAbout.setIcon(icon12)
        self.btnAbout.setIconSize(QSize(50, 70))
        self.btnAbout.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnAbout.setAutoRaise(True)

        self.horizontalLayout_3.addWidget(self.btnAbout)


        self.verticalLayout.addWidget(self.headerFrame)

        self.frame_4 = QFrame(self.mainFrame)
        self.frame_4.setObjectName(u"frame_4")
        self.frame_4.setFrameShape(QFrame.NoFrame)
        self.frame_4.setFrameShadow(QFrame.Plain)
        self.horizontalLayout_2 = QHBoxLayout(self.frame_4)
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 5, 5, 5)
        self.imageFrame = QFrame(self.frame_4)
        self.imageFrame.setObjectName(u"imageFrame")
        self.imageFrame.setFrameShape(QFrame.NoFrame)
        self.imageFrame.setFrameShadow(QFrame.Plain)
        self.imageFrame.setLineWidth(0)
        self.imageLayout = QVBoxLayout(self.imageFrame)
        self.imageLayout.setSpacing(0)
        self.imageLayout.setObjectName(u"imageLayout")
        self.imageLayout.setContentsMargins(0, 0, 20, 0)
        self.btnSelect = QToolButton(self.imageFrame)
        self.btnSelect.setObjectName(u"btnSelect")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.btnSelect.sizePolicy().hasHeightForWidth())
        self.btnSelect.setSizePolicy(sizePolicy3)
        self.btnSelect.setStyleSheet(u"border: none;")
        icon13 = QIcon()
        icon13.addFile(u":/img/img/Group 161.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnSelect.setIcon(icon13)
        self.btnSelect.setIconSize(QSize(300, 200))
        self.btnSelect.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnSelect.setAutoRaise(True)

        self.imageLayout.addWidget(self.btnSelect)


        self.horizontalLayout_2.addWidget(self.imageFrame)

        self.rightMenu = QFrame(self.frame_4)
        self.rightMenu.setObjectName(u"rightMenu")
        sizePolicy.setHeightForWidth(self.rightMenu.sizePolicy().hasHeightForWidth())
        self.rightMenu.setSizePolicy(sizePolicy)
        self.rightMenu.setMinimumSize(QSize(250, 0))
        self.rightMenu.setMaximumSize(QSize(250, 16777215))
        self.rightMenu.setStyleSheet(u"#rightMenu {\n"
"  background-color: rgb(17, 17, 17);\n"
"}")
        self.rightMenu.setFrameShape(QFrame.NoFrame)
        self.rightMenu.setFrameShadow(QFrame.Raised)
        self.verticalLayout_15 = QVBoxLayout(self.rightMenu)
        self.verticalLayout_15.setObjectName(u"verticalLayout_15")
        self.verticalLayout_15.setContentsMargins(0, 0, 0, 0)
        self.rightMenuItems = QWidget(self.rightMenu)
        self.rightMenuItems.setObjectName(u"rightMenuItems")
        self.verticalLayout_3 = QVBoxLayout(self.rightMenuItems)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.frame = QFrame(self.rightMenuItems)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.NoFrame)
        self.frame.setFrameShadow(QFrame.Plain)
        self.verticalLayout_4 = QVBoxLayout(self.frame)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(10, 0, 10, 0)
        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.label_3 = QLabel(self.frame)
        self.label_3.setObjectName(u"label_3")
        sizePolicy4 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy4)
        font = QFont()
        font.setPointSize(10)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet(u"color: rgb(255, 255, 255);")

        self.horizontalLayout_4.addWidget(self.label_3)

        self.txtBrightness = QLineEdit(self.frame)
        self.txtBrightness.setObjectName(u"txtBrightness")
        sizePolicy5 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.txtBrightness.sizePolicy().hasHeightForWidth())
        self.txtBrightness.setSizePolicy(sizePolicy5)
        self.txtBrightness.setMinimumSize(QSize(50, 0))
        self.txtBrightness.setMaximumSize(QSize(50, 16777215))
        self.txtBrightness.setFont(font)

        self.horizontalLayout_4.addWidget(self.txtBrightness)


        self.verticalLayout_4.addLayout(self.horizontalLayout_4)

        self.slideBrightness = QSlider(self.frame)
        self.slideBrightness.setObjectName(u"slideBrightness")
        self.slideBrightness.setStyleSheet(u"QSlider::handle:horizontal {\n"
"    image: url(:/img/img/Path 119.png);\n"
"	width: 100px; height:120px;\n"
"}\n"
"")
        self.slideBrightness.setOrientation(Qt.Horizontal)

        self.verticalLayout_4.addWidget(self.slideBrightness)


        self.verticalLayout_3.addWidget(self.frame)

        self.frame_2 = QFrame(self.rightMenuItems)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setFrameShape(QFrame.NoFrame)
        self.frame_2.setFrameShadow(QFrame.Plain)
        self.verticalLayout_5 = QVBoxLayout(self.frame_2)
        self.verticalLayout_5.setSpacing(0)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(10, 0, 10, 0)
        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.label_4 = QLabel(self.frame_2)
        self.label_4.setObjectName(u"label_4")
        sizePolicy4.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy4)
        self.label_4.setFont(font)
        self.label_4.setStyleSheet(u"color: rgb(255, 255, 255);")

        self.horizontalLayout_5.addWidget(self.label_4)

        self.txtConstrast = QLineEdit(self.frame_2)
        self.txtConstrast.setObjectName(u"txtConstrast")
        sizePolicy5.setHeightForWidth(self.txtConstrast.sizePolicy().hasHeightForWidth())
        self.txtConstrast.setSizePolicy(sizePolicy5)
        self.txtConstrast.setMinimumSize(QSize(50, 0))
        self.txtConstrast.setMaximumSize(QSize(50, 16777215))
        self.txtConstrast.setFont(font)

        self.horizontalLayout_5.addWidget(self.txtConstrast)


        self.verticalLayout_5.addLayout(self.horizontalLayout_5)

        self.slideConstrast = QSlider(self.frame_2)
        self.slideConstrast.setObjectName(u"slideConstrast")
        self.slideConstrast.setStyleSheet(u"QSlider::handle:horizontal {\n"
"    image: url(:/img/img/Path 119.png);\n"
"	width: 100px; height:120px;\n"
"}\n"
"")
        self.slideConstrast.setOrientation(Qt.Horizontal)

        self.verticalLayout_5.addWidget(self.slideConstrast)


        self.verticalLayout_3.addWidget(self.frame_2)

        self.frame_3 = QFrame(self.rightMenuItems)
        self.frame_3.setObjectName(u"frame_3")
        self.frame_3.setFrameShape(QFrame.NoFrame)
        self.frame_3.setFrameShadow(QFrame.Plain)
        self.verticalLayout_6 = QVBoxLayout(self.frame_3)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.verticalLayout_6.setContentsMargins(10, 0, 10, 0)
        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.label_5 = QLabel(self.frame_3)
        self.label_5.setObjectName(u"label_5")
        sizePolicy4.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy4)
        self.label_5.setFont(font)
        self.label_5.setStyleSheet(u"color: rgb(255, 255, 255);")

        self.horizontalLayout_6.addWidget(self.label_5)

        self.txtSaturation = QLineEdit(self.frame_3)
        self.txtSaturation.setObjectName(u"txtSaturation")
        sizePolicy5.setHeightForWidth(self.txtSaturation.sizePolicy().hasHeightForWidth())
        self.txtSaturation.setSizePolicy(sizePolicy5)
        self.txtSaturation.setMinimumSize(QSize(50, 0))
        self.txtSaturation.setMaximumSize(QSize(50, 16777215))
        self.txtSaturation.setFont(font)

        self.horizontalLayout_6.addWidget(self.txtSaturation)


        self.verticalLayout_6.addLayout(self.horizontalLayout_6)

        self.slideSaturation = QSlider(self.frame_3)
        self.slideSaturation.setObjectName(u"slideSaturation")
        self.slideSaturation.setStyleSheet(u"QSlider::handle:horizontal {\n"
"    image: url(:/img/img/Path 119.png);\n"
"	width: 100px; height:120px;\n"
"}\n"
"")
        self.slideSaturation.setOrientation(Qt.Horizontal)

        self.verticalLayout_6.addWidget(self.slideSaturation)


        self.verticalLayout_3.addWidget(self.frame_3)

        self.label_6 = QLabel(self.rightMenuItems)
        self.label_6.setObjectName(u"label_6")
        sizePolicy2.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy2)
        self.label_6.setMinimumSize(QSize(0, 30))
        font1 = QFont()
        font1.setPointSize(10)
        font1.setBold(True)
        font1.setWeight(75)
        self.label_6.setFont(font1)
        self.label_6.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);")
        self.label_6.setAlignment(Qt.AlignBottom|Qt.AlignLeading|Qt.AlignLeft)

        self.verticalLayout_3.addWidget(self.label_6)

        self.frame_6 = QFrame(self.rightMenuItems)
        self.frame_6.setObjectName(u"frame_6")
        self.frame_6.setFrameShape(QFrame.NoFrame)
        self.frame_6.setFrameShadow(QFrame.Plain)
        self.verticalLayout_8 = QVBoxLayout(self.frame_6)
        self.verticalLayout_8.setSpacing(0)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_8.setContentsMargins(10, 0, 10, 0)
        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.label_8 = QLabel(self.frame_6)
        self.label_8.setObjectName(u"label_8")
        sizePolicy4.setHeightForWidth(self.label_8.sizePolicy().hasHeightForWidth())
        self.label_8.setSizePolicy(sizePolicy4)
        self.label_8.setFont(font)
        self.label_8.setStyleSheet(u"color: rgb(255, 255, 255);")

        self.horizontalLayout_8.addWidget(self.label_8)

        self.txtHintensity = QLineEdit(self.frame_6)
        self.txtHintensity.setObjectName(u"txtHintensity")
        sizePolicy5.setHeightForWidth(self.txtHintensity.sizePolicy().hasHeightForWidth())
        self.txtHintensity.setSizePolicy(sizePolicy5)
        self.txtHintensity.setMinimumSize(QSize(50, 0))
        self.txtHintensity.setMaximumSize(QSize(50, 16777215))
        self.txtHintensity.setFont(font)

        self.horizontalLayout_8.addWidget(self.txtHintensity)


        self.verticalLayout_8.addLayout(self.horizontalLayout_8)

        self.slideHintensity = QSlider(self.frame_6)
        self.slideHintensity.setObjectName(u"slideHintensity")
        self.slideHintensity.setStyleSheet(u"QSlider::handle:horizontal {\n"
"    image: url(:/img/img/Path 119.png);\n"
"	width: 100px; height:120px;\n"
"}\n"
"")
        self.slideHintensity.setOrientation(Qt.Horizontal)

        self.verticalLayout_8.addWidget(self.slideHintensity)


        self.verticalLayout_3.addWidget(self.frame_6)

        self.frame_7 = QFrame(self.rightMenuItems)
        self.frame_7.setObjectName(u"frame_7")
        self.frame_7.setFrameShape(QFrame.NoFrame)
        self.frame_7.setFrameShadow(QFrame.Plain)
        self.verticalLayout_9 = QVBoxLayout(self.frame_7)
        self.verticalLayout_9.setSpacing(0)
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.verticalLayout_9.setContentsMargins(10, 0, 10, 0)
        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.label_9 = QLabel(self.frame_7)
        self.label_9.setObjectName(u"label_9")
        sizePolicy4.setHeightForWidth(self.label_9.sizePolicy().hasHeightForWidth())
        self.label_9.setSizePolicy(sizePolicy4)
        self.label_9.setFont(font)
        self.label_9.setStyleSheet(u"color: rgb(255, 255, 255);")

        self.horizontalLayout_9.addWidget(self.label_9)

        self.txtSintensity = QLineEdit(self.frame_7)
        self.txtSintensity.setObjectName(u"txtSintensity")
        sizePolicy5.setHeightForWidth(self.txtSintensity.sizePolicy().hasHeightForWidth())
        self.txtSintensity.setSizePolicy(sizePolicy5)
        self.txtSintensity.setMinimumSize(QSize(50, 0))
        self.txtSintensity.setMaximumSize(QSize(50, 16777215))
        self.txtSintensity.setFont(font)

        self.horizontalLayout_9.addWidget(self.txtSintensity)


        self.verticalLayout_9.addLayout(self.horizontalLayout_9)

        self.slideSintensity = QSlider(self.frame_7)
        self.slideSintensity.setObjectName(u"slideSintensity")
        self.slideSintensity.setStyleSheet(u"QSlider::handle:horizontal {\n"
"    image: url(:/img/img/Path 119.png);\n"
"	width: 100px; height:120px;\n"
"}\n"
"")
        self.slideSintensity.setOrientation(Qt.Horizontal)

        self.verticalLayout_9.addWidget(self.slideSintensity)


        self.verticalLayout_3.addWidget(self.frame_7)

        self.frame_5 = QFrame(self.rightMenuItems)
        self.frame_5.setObjectName(u"frame_5")
        self.frame_5.setFrameShape(QFrame.NoFrame)
        self.frame_5.setFrameShadow(QFrame.Plain)
        self.verticalLayout_7 = QVBoxLayout(self.frame_5)
        self.verticalLayout_7.setSpacing(0)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.verticalLayout_7.setContentsMargins(10, 0, 10, 0)
        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.label_7 = QLabel(self.frame_5)
        self.label_7.setObjectName(u"label_7")
        sizePolicy4.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy4)
        self.label_7.setFont(font)
        self.label_7.setStyleSheet(u"color: rgb(255, 255, 255);")

        self.horizontalLayout_7.addWidget(self.label_7)

        self.txtVintensity = QLineEdit(self.frame_5)
        self.txtVintensity.setObjectName(u"txtVintensity")
        sizePolicy5.setHeightForWidth(self.txtVintensity.sizePolicy().hasHeightForWidth())
        self.txtVintensity.setSizePolicy(sizePolicy5)
        self.txtVintensity.setMinimumSize(QSize(50, 0))
        self.txtVintensity.setMaximumSize(QSize(50, 16777215))
        self.txtVintensity.setFont(font)

        self.horizontalLayout_7.addWidget(self.txtVintensity)


        self.verticalLayout_7.addLayout(self.horizontalLayout_7)

        self.slideVintensity = QSlider(self.frame_5)
        self.slideVintensity.setObjectName(u"slideVintensity")
        self.slideVintensity.setStyleSheet(u"QSlider::handle:horizontal {\n"
"    image: url(:/img/img/Path 119.png);\n"
"	width: 100px; height:120px;\n"
"}\n"
"")
        self.slideVintensity.setOrientation(Qt.Horizontal)

        self.verticalLayout_7.addWidget(self.slideVintensity)


        self.verticalLayout_3.addWidget(self.frame_5)

        self.label_13 = QLabel(self.rightMenuItems)
        self.label_13.setObjectName(u"label_13")
        sizePolicy2.setHeightForWidth(self.label_13.sizePolicy().hasHeightForWidth())
        self.label_13.setSizePolicy(sizePolicy2)
        self.label_13.setMinimumSize(QSize(0, 30))
        self.label_13.setFont(font1)
        self.label_13.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);")
        self.label_13.setAlignment(Qt.AlignBottom|Qt.AlignLeading|Qt.AlignLeft)

        self.verticalLayout_3.addWidget(self.label_13)

        self.frame_9 = QFrame(self.rightMenuItems)
        self.frame_9.setObjectName(u"frame_9")
        self.frame_9.setFrameShape(QFrame.NoFrame)
        self.frame_9.setFrameShadow(QFrame.Plain)
        self.verticalLayout_11 = QVBoxLayout(self.frame_9)
        self.verticalLayout_11.setSpacing(0)
        self.verticalLayout_11.setObjectName(u"verticalLayout_11")
        self.verticalLayout_11.setContentsMargins(10, 0, 10, 0)
        self.horizontalLayout_11 = QHBoxLayout()
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.label_11 = QLabel(self.frame_9)
        self.label_11.setObjectName(u"label_11")
        sizePolicy4.setHeightForWidth(self.label_11.sizePolicy().hasHeightForWidth())
        self.label_11.setSizePolicy(sizePolicy4)
        self.label_11.setFont(font)
        self.label_11.setStyleSheet(u"color: rgb(255, 255, 255);")

        self.horizontalLayout_11.addWidget(self.label_11)

        self.txtAmount = QLineEdit(self.frame_9)
        self.txtAmount.setObjectName(u"txtAmount")
        sizePolicy5.setHeightForWidth(self.txtAmount.sizePolicy().hasHeightForWidth())
        self.txtAmount.setSizePolicy(sizePolicy5)
        self.txtAmount.setMinimumSize(QSize(50, 0))
        self.txtAmount.setMaximumSize(QSize(50, 16777215))
        self.txtAmount.setFont(font)

        self.horizontalLayout_11.addWidget(self.txtAmount)


        self.verticalLayout_11.addLayout(self.horizontalLayout_11)

        self.slideAmount = QSlider(self.frame_9)
        self.slideAmount.setObjectName(u"slideAmount")
        self.slideAmount.setStyleSheet(u"QSlider::handle:horizontal {\n"
"    image: url(:/img/img/Path 119.png);\n"
"	width: 100px; height:120px;\n"
"}\n"
"")
        self.slideAmount.setOrientation(Qt.Horizontal)

        self.verticalLayout_11.addWidget(self.slideAmount)


        self.verticalLayout_3.addWidget(self.frame_9)

        self.frame_10 = QFrame(self.rightMenuItems)
        self.frame_10.setObjectName(u"frame_10")
        self.frame_10.setFrameShape(QFrame.NoFrame)
        self.frame_10.setFrameShadow(QFrame.Plain)
        self.verticalLayout_12 = QVBoxLayout(self.frame_10)
        self.verticalLayout_12.setSpacing(0)
        self.verticalLayout_12.setObjectName(u"verticalLayout_12")
        self.verticalLayout_12.setContentsMargins(10, 0, 10, 0)
        self.horizontalLayout_12 = QHBoxLayout()
        self.horizontalLayout_12.setObjectName(u"horizontalLayout_12")
        self.label_12 = QLabel(self.frame_10)
        self.label_12.setObjectName(u"label_12")
        sizePolicy4.setHeightForWidth(self.label_12.sizePolicy().hasHeightForWidth())
        self.label_12.setSizePolicy(sizePolicy4)
        self.label_12.setFont(font)
        self.label_12.setStyleSheet(u"color: rgb(255, 255, 255);")

        self.horizontalLayout_12.addWidget(self.label_12)

        self.txtRadius = QLineEdit(self.frame_10)
        self.txtRadius.setObjectName(u"txtRadius")
        sizePolicy5.setHeightForWidth(self.txtRadius.sizePolicy().hasHeightForWidth())
        self.txtRadius.setSizePolicy(sizePolicy5)
        self.txtRadius.setMinimumSize(QSize(50, 0))
        self.txtRadius.setMaximumSize(QSize(50, 16777215))
        self.txtRadius.setFont(font)

        self.horizontalLayout_12.addWidget(self.txtRadius)


        self.verticalLayout_12.addLayout(self.horizontalLayout_12)

        self.slideRadius = QSlider(self.frame_10)
        self.slideRadius.setObjectName(u"slideRadius")
        self.slideRadius.setStyleSheet(u"QSlider::handle:horizontal {\n"
"    image: url(:/img/img/Path 119.png);\n"
"	width: 100px; height:120px;\n"
"}\n"
"")
        self.slideRadius.setOrientation(Qt.Horizontal)

        self.verticalLayout_12.addWidget(self.slideRadius)


        self.verticalLayout_3.addWidget(self.frame_10)

        self.frame_8 = QFrame(self.rightMenuItems)
        self.frame_8.setObjectName(u"frame_8")
        self.frame_8.setFrameShape(QFrame.NoFrame)
        self.frame_8.setFrameShadow(QFrame.Plain)
        self.verticalLayout_10 = QVBoxLayout(self.frame_8)
        self.verticalLayout_10.setSpacing(0)
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.verticalLayout_10.setContentsMargins(10, 0, 10, 0)
        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.label_10 = QLabel(self.frame_8)
        self.label_10.setObjectName(u"label_10")
        sizePolicy4.setHeightForWidth(self.label_10.sizePolicy().hasHeightForWidth())
        self.label_10.setSizePolicy(sizePolicy4)
        self.label_10.setFont(font)
        self.label_10.setStyleSheet(u"color: rgb(255, 255, 255);")

        self.horizontalLayout_10.addWidget(self.label_10)

        self.txtThreshold = QLineEdit(self.frame_8)
        self.txtThreshold.setObjectName(u"txtThreshold")
        sizePolicy5.setHeightForWidth(self.txtThreshold.sizePolicy().hasHeightForWidth())
        self.txtThreshold.setSizePolicy(sizePolicy5)
        self.txtThreshold.setMinimumSize(QSize(50, 0))
        self.txtThreshold.setMaximumSize(QSize(50, 16777215))
        self.txtThreshold.setFont(font)

        self.horizontalLayout_10.addWidget(self.txtThreshold)


        self.verticalLayout_10.addLayout(self.horizontalLayout_10)

        self.slideThreshold = QSlider(self.frame_8)
        self.slideThreshold.setObjectName(u"slideThreshold")
        self.slideThreshold.setStyleSheet(u"QSlider::handle:horizontal {\n"
"    image: url(:/img/img/Path 119.png);\n"
"	width: 100px; height:120px;\n"
"}\n"
"")
        self.slideThreshold.setOrientation(Qt.Horizontal)

        self.verticalLayout_10.addWidget(self.slideThreshold)


        self.verticalLayout_3.addWidget(self.frame_8)

        self.label_14 = QLabel(self.rightMenuItems)
        self.label_14.setObjectName(u"label_14")
        sizePolicy2.setHeightForWidth(self.label_14.sizePolicy().hasHeightForWidth())
        self.label_14.setSizePolicy(sizePolicy2)
        self.label_14.setMinimumSize(QSize(0, 30))
        self.label_14.setFont(font1)
        self.label_14.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);")
        self.label_14.setAlignment(Qt.AlignBottom|Qt.AlignLeading|Qt.AlignLeft)

        self.verticalLayout_3.addWidget(self.label_14)

        self.frame_11 = QFrame(self.rightMenuItems)
        self.frame_11.setObjectName(u"frame_11")
        self.frame_11.setFrameShape(QFrame.NoFrame)
        self.frame_11.setFrameShadow(QFrame.Plain)
        self.verticalLayout_13 = QVBoxLayout(self.frame_11)
        self.verticalLayout_13.setSpacing(0)
        self.verticalLayout_13.setObjectName(u"verticalLayout_13")
        self.verticalLayout_13.setContentsMargins(10, 0, 10, 0)
        self.horizontalLayout_13 = QHBoxLayout()
        self.horizontalLayout_13.setObjectName(u"horizontalLayout_13")
        self.label_15 = QLabel(self.frame_11)
        self.label_15.setObjectName(u"label_15")
        sizePolicy4.setHeightForWidth(self.label_15.sizePolicy().hasHeightForWidth())
        self.label_15.setSizePolicy(sizePolicy4)
        self.label_15.setFont(font)
        self.label_15.setStyleSheet(u"color: rgb(255, 255, 255);")

        self.horizontalLayout_13.addWidget(self.label_15)

        self.txtRadiusX = QLineEdit(self.frame_11)
        self.txtRadiusX.setObjectName(u"txtRadiusX")
        sizePolicy5.setHeightForWidth(self.txtRadiusX.sizePolicy().hasHeightForWidth())
        self.txtRadiusX.setSizePolicy(sizePolicy5)
        self.txtRadiusX.setMinimumSize(QSize(50, 0))
        self.txtRadiusX.setMaximumSize(QSize(50, 16777215))
        self.txtRadiusX.setFont(font)

        self.horizontalLayout_13.addWidget(self.txtRadiusX)


        self.verticalLayout_13.addLayout(self.horizontalLayout_13)

        self.slideRadiusX = QSlider(self.frame_11)
        self.slideRadiusX.setObjectName(u"slideRadiusX")
        self.slideRadiusX.setStyleSheet(u"QSlider::handle:horizontal {\n"
"    image: url(:/img/img/Path 119.png);\n"
"	width: 100px; height:120px;\n"
"}\n"
"")
        self.slideRadiusX.setOrientation(Qt.Horizontal)

        self.verticalLayout_13.addWidget(self.slideRadiusX)


        self.verticalLayout_3.addWidget(self.frame_11)

        self.frame_12 = QFrame(self.rightMenuItems)
        self.frame_12.setObjectName(u"frame_12")
        self.frame_12.setFrameShape(QFrame.NoFrame)
        self.frame_12.setFrameShadow(QFrame.Plain)
        self.verticalLayout_14 = QVBoxLayout(self.frame_12)
        self.verticalLayout_14.setSpacing(0)
        self.verticalLayout_14.setObjectName(u"verticalLayout_14")
        self.verticalLayout_14.setContentsMargins(10, 0, 10, 0)
        self.horizontalLayout_14 = QHBoxLayout()
        self.horizontalLayout_14.setObjectName(u"horizontalLayout_14")
        self.label_16 = QLabel(self.frame_12)
        self.label_16.setObjectName(u"label_16")
        sizePolicy4.setHeightForWidth(self.label_16.sizePolicy().hasHeightForWidth())
        self.label_16.setSizePolicy(sizePolicy4)
        self.label_16.setFont(font)
        self.label_16.setStyleSheet(u"color: rgb(255, 255, 255);")

        self.horizontalLayout_14.addWidget(self.label_16)

        self.txtRadiusY = QLineEdit(self.frame_12)
        self.txtRadiusY.setObjectName(u"txtRadiusY")
        sizePolicy5.setHeightForWidth(self.txtRadiusY.sizePolicy().hasHeightForWidth())
        self.txtRadiusY.setSizePolicy(sizePolicy5)
        self.txtRadiusY.setMinimumSize(QSize(50, 0))
        self.txtRadiusY.setMaximumSize(QSize(50, 16777215))
        self.txtRadiusY.setFont(font)

        self.horizontalLayout_14.addWidget(self.txtRadiusY)


        self.verticalLayout_14.addLayout(self.horizontalLayout_14)

        self.slideRadiusY = QSlider(self.frame_12)
        self.slideRadiusY.setObjectName(u"slideRadiusY")
        self.slideRadiusY.setStyleSheet(u"QSlider::handle:horizontal {\n"
"    image: url(:/img/img/Path 119.png);\n"
"	width: 100px; height:120px;\n"
"}\n"
"")
        self.slideRadiusY.setOrientation(Qt.Horizontal)

        self.verticalLayout_14.addWidget(self.slideRadiusY)


        self.verticalLayout_3.addWidget(self.frame_12)

        self.label_17 = QLabel(self.rightMenuItems)
        self.label_17.setObjectName(u"label_17")
        sizePolicy2.setHeightForWidth(self.label_17.sizePolicy().hasHeightForWidth())
        self.label_17.setSizePolicy(sizePolicy2)
        self.label_17.setMinimumSize(QSize(0, 30))
        self.label_17.setFont(font1)
        self.label_17.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);")

        self.verticalLayout_3.addWidget(self.label_17)


        self.verticalLayout_15.addWidget(self.rightMenuItems)

        self.btnSave = QToolButton(self.rightMenu)
        self.btnSave.setObjectName(u"btnSave")
        sizePolicy1.setHeightForWidth(self.btnSave.sizePolicy().hasHeightForWidth())
        self.btnSave.setSizePolicy(sizePolicy1)
        self.btnSave.setStyleSheet(u"border: none;")
        icon14 = QIcon()
        icon14.addFile(u":/img/img/Group 58.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnSave.setIcon(icon14)
        self.btnSave.setIconSize(QSize(200, 50))
        self.btnSave.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnSave.setAutoRaise(True)

        self.verticalLayout_15.addWidget(self.btnSave)


        self.horizontalLayout_2.addWidget(self.rightMenu)


        self.verticalLayout.addWidget(self.frame_4)

        self.bottomBanner = QWidget(self.mainFrame)
        self.bottomBanner.setObjectName(u"bottomBanner")
        sizePolicy2.setHeightForWidth(self.bottomBanner.sizePolicy().hasHeightForWidth())
        self.bottomBanner.setSizePolicy(sizePolicy2)
        self.bottomBanner.setMinimumSize(QSize(0, 15))
        self.bottomBanner.setMaximumSize(QSize(16777215, 15))
        self.bottomBanner.setStyleSheet(u"#bottomBanner {\n"
"border-image: url(:/img/img/Rectangle 23.png);\n"
"}")
        self.horizontalLayout_15 = QHBoxLayout(self.bottomBanner)
        self.horizontalLayout_15.setObjectName(u"horizontalLayout_15")
        self.horizontalLayout_15.setContentsMargins(0, 0, 0, 0)
        self.progressBar = QProgressBar(self.bottomBanner)
        self.progressBar.setObjectName(u"progressBar")
        self.progressBar.setMinimumSize(QSize(0, 30))
        self.progressBar.setStyleSheet(u"QProgressBar\n"
"{\n"
"background-color : rgba(0, 0, 0, 0);\n"
"border : 1px;\n"
"}")
        self.progressBar.setValue(0)

        self.horizontalLayout_15.addWidget(self.progressBar)


        self.verticalLayout.addWidget(self.bottomBanner)


        self.horizontalLayout.addWidget(self.mainFrame)


        self.retranslateUi(ColorizeLife)

        QMetaObject.connectSlotsByName(ColorizeLife)
    # setupUi

    def retranslateUi(self, ColorizeLife):
        ColorizeLife.setWindowTitle(QCoreApplication.translate("ColorizeLife", u"Colorize LIFE", None))
        self.btnProcess.setText("")
        self.btnClear.setText("")
        self.btnAdd.setText("")
        self.btnMove.setText("")
        self.btnRestore.setText("")
        self.btnCrop.setText("")
        self.btnColorize.setText("")
        self.btnImpaint.setText("")
        self.btnFitscreen.setText("")
        self.btnZoomin.setText("")
        self.btnZoomout.setText("")
        self.label.setText("")
        self.btnOptions.setText("")
        self.btnAbout.setText("")
        self.btnSelect.setText("")
        self.label_3.setText(QCoreApplication.translate("ColorizeLife", u"Brightness", None))
        self.txtBrightness.setText("")
        self.label_4.setText(QCoreApplication.translate("ColorizeLife", u"Contrast", None))
        self.label_5.setText(QCoreApplication.translate("ColorizeLife", u"Saturation", None))
        self.label_6.setText(QCoreApplication.translate("ColorizeLife", u"HSV ADJUSTMENT", None))
        self.label_8.setText(QCoreApplication.translate("ColorizeLife", u"Hintensity", None))
        self.label_9.setText(QCoreApplication.translate("ColorizeLife", u"Sintensity", None))
        self.label_7.setText(QCoreApplication.translate("ColorizeLife", u"Vintensity", None))
        self.label_13.setText(QCoreApplication.translate("ColorizeLife", u"SHARPNESS", None))
        self.label_11.setText(QCoreApplication.translate("ColorizeLife", u"Amount", None))
        self.label_12.setText(QCoreApplication.translate("ColorizeLife", u"Radius", None))
        self.label_10.setText(QCoreApplication.translate("ColorizeLife", u"Threshold", None))
        self.label_14.setText(QCoreApplication.translate("ColorizeLife", u"DENOISE", None))
        self.label_15.setText(QCoreApplication.translate("ColorizeLife", u"Radius X", None))
        self.label_16.setText(QCoreApplication.translate("ColorizeLife", u"Radius Y", None))
        self.label_17.setText("")
        self.btnSave.setText("")
    # retranslateUi

