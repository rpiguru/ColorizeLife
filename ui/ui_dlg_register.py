# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'dlg_register.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import ui.cf_rc

class Ui_RegisterDialog(object):
    def setupUi(self, RegisterDialog):
        if not RegisterDialog.objectName():
            RegisterDialog.setObjectName(u"RegisterDialog")
        RegisterDialog.setWindowModality(Qt.ApplicationModal)
        RegisterDialog.resize(728, 455)
        RegisterDialog.setAutoFillBackground(False)
        RegisterDialog.setStyleSheet(u"#RegisterDialog {\n"
"background-color: rgb(17, 17, 17);\n"
"}")
        RegisterDialog.setModal(True)
        self.widget = QWidget(RegisterDialog)
        self.widget.setObjectName(u"widget")
        self.widget.setGeometry(QRect(60, 20, 655, 257))
        self.horizontalLayout_2 = QHBoxLayout(self.widget)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.label = QLabel(self.widget)
        self.label.setObjectName(u"label")
        self.label.setPixmap(QPixmap(u":/img/img/Component 13 \u2013 1.png"))

        self.horizontalLayout_2.addWidget(self.label)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.btnClose = QToolButton(self.widget)
        self.btnClose.setObjectName(u"btnClose")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnClose.sizePolicy().hasHeightForWidth())
        self.btnClose.setSizePolicy(sizePolicy)
        self.btnClose.setStyleSheet(u"border: none;")
        icon = QIcon()
        icon.addFile(u":/img/img/x.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnClose.setIcon(icon)
        self.btnClose.setIconSize(QSize(50, 70))
        self.btnClose.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnClose.setAutoRaise(True)

        self.verticalLayout.addWidget(self.btnClose)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)


        self.horizontalLayout_2.addLayout(self.verticalLayout)

        self.widget1 = QWidget(RegisterDialog)
        self.widget1.setObjectName(u"widget1")
        self.widget1.setGeometry(QRect(160, 290, 452, 133))
        self.verticalLayout_2 = QVBoxLayout(self.widget1)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.txtLicenseKey = QLineEdit(self.widget1)
        self.txtLicenseKey.setObjectName(u"txtLicenseKey")
        self.txtLicenseKey.setMinimumSize(QSize(450, 50))
        self.txtLicenseKey.setMaximumSize(QSize(200, 16777215))
        font = QFont()
        font.setPointSize(16)
        self.txtLicenseKey.setFont(font)

        self.verticalLayout_2.addWidget(self.txtLicenseKey)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setSpacing(20)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.btnCancel = QToolButton(self.widget1)
        self.btnCancel.setObjectName(u"btnCancel")
        sizePolicy.setHeightForWidth(self.btnCancel.sizePolicy().hasHeightForWidth())
        self.btnCancel.setSizePolicy(sizePolicy)
        self.btnCancel.setMinimumSize(QSize(150, 55))
        font1 = QFont()
        font1.setPointSize(18)
        self.btnCancel.setFont(font1)
        self.btnCancel.setStyleSheet(u"border: none;\n"
"color: rgb(255, 255, 255);")
        self.btnCancel.setIconSize(QSize(50, 70))
        self.btnCancel.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnCancel.setAutoRaise(True)

        self.horizontalLayout.addWidget(self.btnCancel)

        self.btnRegister = QToolButton(self.widget1)
        self.btnRegister.setObjectName(u"btnRegister")
        sizePolicy.setHeightForWidth(self.btnRegister.sizePolicy().hasHeightForWidth())
        self.btnRegister.setSizePolicy(sizePolicy)
        self.btnRegister.setStyleSheet(u"border: none;")
        icon1 = QIcon()
        icon1.addFile(u":/img/img/Component 14 \u2013 1.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnRegister.setIcon(icon1)
        self.btnRegister.setIconSize(QSize(200, 70))
        self.btnRegister.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnRegister.setAutoRaise(True)

        self.horizontalLayout.addWidget(self.btnRegister)


        self.verticalLayout_2.addLayout(self.horizontalLayout)


        self.retranslateUi(RegisterDialog)

        QMetaObject.connectSlotsByName(RegisterDialog)
    # setupUi

    def retranslateUi(self, RegisterDialog):
        RegisterDialog.setWindowTitle(QCoreApplication.translate("RegisterDialog", u"Dialog", None))
        self.label.setText("")
        self.btnClose.setText("")
        self.txtLicenseKey.setText("")
        self.btnCancel.setText(QCoreApplication.translate("RegisterDialog", u"Cancel", None))
        self.btnRegister.setText("")
    # retranslateUi

