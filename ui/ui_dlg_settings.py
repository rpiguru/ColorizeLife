# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'dlg_settings.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import ui.cf_rc

class Ui_SettingsDialog(object):
    def setupUi(self, SettingsDialog):
        if not SettingsDialog.objectName():
            SettingsDialog.setObjectName(u"SettingsDialog")
        SettingsDialog.resize(673, 310)
        SettingsDialog.setStyleSheet(u"#SettingsDialog {\n"
"  background-color: rgb(17, 17, 17);\n"
"}\n"
"")
        self.verticalLayout = QVBoxLayout(SettingsDialog)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(-1, -1, -1, 100)
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_5)

        self.btnClose = QToolButton(SettingsDialog)
        self.btnClose.setObjectName(u"btnClose")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnClose.sizePolicy().hasHeightForWidth())
        self.btnClose.setSizePolicy(sizePolicy)
        self.btnClose.setMinimumSize(QSize(0, 40))
        self.btnClose.setMaximumSize(QSize(16777215, 50))
        self.btnClose.setStyleSheet(u"border: none;")
        icon = QIcon()
        icon.addFile(u":/img/img/x.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnClose.setIcon(icon)
        self.btnClose.setIconSize(QSize(50, 70))
        self.btnClose.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.btnClose.setAutoRaise(True)

        self.horizontalLayout_3.addWidget(self.btnClose)


        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_4)

        self.label = QLabel(SettingsDialog)
        self.label.setObjectName(u"label")
        self.label.setPixmap(QPixmap(u":/img/img/Component 17 \u2013 1.png"))

        self.horizontalLayout.addWidget(self.label)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_3)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setSpacing(50)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(-1, 50, -1, -1)
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.radioPNG = QRadioButton(SettingsDialog)
        self.radioPNG.setObjectName(u"radioPNG")
        font = QFont()
        font.setPointSize(20)
        self.radioPNG.setFont(font)
        self.radioPNG.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"QRadioButton::checked { \n"
"	border-image: url(:/img/img/Group 162.png) 0 0 0 0 stretch stretch; \n"
"}\n"
"")

        self.horizontalLayout_2.addWidget(self.radioPNG)

        self.radioJPEG = QRadioButton(SettingsDialog)
        self.radioJPEG.setObjectName(u"radioJPEG")
        self.radioJPEG.setFont(font)
        self.radioJPEG.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"QRadioButton::checked { \n"
"	border-image: url(:/img/img/Group 162.png) 0 0 0 0 stretch stretch; \n"
"}\n"
"")

        self.horizontalLayout_2.addWidget(self.radioJPEG)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_2)


        self.verticalLayout.addLayout(self.horizontalLayout_2)


        self.retranslateUi(SettingsDialog)

        QMetaObject.connectSlotsByName(SettingsDialog)
    # setupUi

    def retranslateUi(self, SettingsDialog):
        SettingsDialog.setWindowTitle(QCoreApplication.translate("SettingsDialog", u"Dialog", None))
        self.btnClose.setText("")
        self.label.setText("")
        self.radioPNG.setText(QCoreApplication.translate("SettingsDialog", u" PNG", None))
        self.radioJPEG.setText(QCoreApplication.translate("SettingsDialog", u" JPEG", None))
    # retranslateUi

