# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['main.py'],
             pathex=['/Volumes/Data/Work/ColorizeLife'],
             binaries=[

             ],
             datas=[
                ('models/ColorizeArtistic_gen.pth', './models/'),
                ('colorize_life.icns', '.')
             ],
             hiddenimports=[
                'pandas._libs.tslibs.base',
             ],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='Colorize LIFE',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False)
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='colorize_life')
app = BUNDLE(coll,
             name='colorize_life.app',
             icon='colorize_life.icns',
             osx_bundle_identifier='com.colorizelife.colorize.colorize',
 	    )
