# ColorizeLife

GUI Application to Colorize Images

## Installation on Windows10

- Windows: Download [Python3.6.8 X86-64](https://www.python.org/ftp/python/3.6.8/python-3.6.8-amd64.exe)

- MacOS: Download from [here](https://www.python.org/ftp/python/3.6.8/python-3.6.8-macosx10.9.pkg)

- Clone this project to your local drive and do the following steps:

    ```shell script
    python -m pip install -U pip setuptools wheel
  
    # For Windows
    python -m pip install -r requirements_win.txt
  
    # For MacOS
    python -m pip install -r requirements_mac.txt
    ```

## Package the application

https://www.learnpyqt.com/tutorials/packaging-pyqt5-pyside2-applications-windows-pyinstaller/

### Windows

- Create an executable: 

    ```shell script
    cp utils\hook-fastprogress.py venv_win\Lib\site-packages\PyInstaller\hooks
    python -m PyInstaller main_windows.spec
    cp -R venv_win\Lib\site-packages\PySide2\plugins\platforms dist\colorize_life
    ```

- Open [colorize_life.ifp](colorize_life.ifp) by [InstallForge](https://installforge.net/) and build a setup file.

### MacOS

- Create an executable:

    ```shell scripts
    cp utils/hook-fastprogress.py venv/lib/python3.6/site-packages/PyInstaller/hooks/
    PyInstaller main_mac.spec
    codesign -f -s - dist/colorize_life.app/Contents/MacOS/Python
    ```
- Create .dmg file from the application: https://kb.parallels.com/123895
