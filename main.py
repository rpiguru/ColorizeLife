import json
import os
import sys
import threading
from datetime import datetime
from functools import partial
import warnings

warnings.filterwarnings("ignore", category=UserWarning, message=".*?Your .*? set is empty.*?")
os.environ["PYTORCH_JIT"] = "0"

from PySide2.QtCore import Signal, QTimer, Qt
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QDialog, QApplication, QFileDialog, QInputDialog, QMessageBox
from settings import REG_FILE
from utils.sl_validate import check_license
from ui.ui_colorize_life import Ui_ColorizeLife
from utils.common import get_config
from utils.logger import logger
from widgets.dialogs.dlg_about import AboutDialog
from widgets.dialogs.dlg_option import OptionDialog
from widgets.dialogs.dlg_register import RegisterDialog
from widgets.image import ImageWidget
from widgets.message import show_message

from deoldify.deoldifier import DeOldifier
import cv2


try:
    # Include in try/except block if you're also targeting Mac/Linux
    from PySide2.QtWinExtras import QtWin
    myappid = 'colorizelife.colorize.life.1'
    QtWin.setCurrentProcessExplicitAppUserModelID(myappid)
except ImportError:
    pass


_cur_dir = os.path.dirname(os.path.realpath(__file__))


class ColorizeLifeApp(QDialog):

    finished = Signal(object)

    def __init__(self):
        super().__init__()
        self.ui = Ui_ColorizeLife()
        self.ui.setupUi(self)
        self.setWindowFlag(Qt.WindowMinimizeButtonHint, True)
        self.setWindowFlag(Qt.WindowMaximizeButtonHint, True)
        self.setWindowFlag(Qt.WindowContextHelpButtonHint, False)
        self._deoldifier = DeOldifier(render_factor=get_config().get('render_factor', 25))
        for btn in {'About', 'Select', 'Colorize', 'Save', 'Process', 'Clear'}:
            getattr(self.ui, f"btn{btn}").released.connect(partial(self._on_btn, btn.lower()))
        for btn in {'Add', 'Move', 'Restore', 'Crop', 'Colorize',
                    'Impaint', 'Fitscreen', 'Zoomin', 'Zoomout', 'Options'}:
            getattr(self.ui, f"btn{btn}").setVisible(False)
        self.ui.rightMenuItems.setVisible(False)
        self._select_btn = self.ui.btnSelect
        self._gray_frame = None
        self._color_frame = None
        self._img_widget = None
        self._render_factor = get_config().get('render_factor', 25)
        self._gray_image_file = None
        self._p_timer = QTimer()
        self._p_timer.timeout.connect(self._update_progress_bar)
        getattr(self, 'finished').connect(self._on_finished)
        self._authorize_app()

    @staticmethod
    def _is_local_license_valid():
        if os.path.exists(REG_FILE):
            reg = json.loads(open(REG_FILE).read())
            expires = datetime.strptime(reg['expires'], "%Y-%m-%d %H:%M:%S")
            return datetime.now() < expires

    def _authorize_app(self):
        # Authorize app
        while not self._is_local_license_valid():
            txt, ok = QInputDialog.getText(self, 'Colorize LIFE',
                                           'Enter the activation key you received via email after your purchase.')
            if ok:
                v_result = check_license(txt.strip())
                if v_result and v_result.get('license') == 'valid':
                    logger.info(f"Successfully authorized")
                    with open(REG_FILE, 'w') as jp:
                        json.dump(v_result, jp, indent=2)
                    break
                else:
                    m = QMessageBox()
                    m.setWindowTitle("Colorize LIFE")
                    m.setIcon(m.Icon.Critical)
                    m.setText("Incorrect key. Please check your email again and enter the correct code.")
                    m.exec_()
            else:
                sys.exit(-1)

    def _on_btn(self, btn):
        if btn == 'about':
            AboutDialog(parent=self).exec_()
        elif btn == 'register':
            RegisterDialog(parent=self).exec_()
        elif btn == 'options':
            OptionDialog(parent=self).exec_()
        elif btn == 'select':
            img_file, _ = QFileDialog.getOpenFileName(caption="Select Gray Image", filter='Image Files (*.jpg *.png)')
            if img_file:
                self._gray_image_file = img_file
                self._select_btn.setParent(None)
                self._img_widget = ImageWidget(parent=self)
                self.ui.imageLayout.addWidget(self._img_widget)
                self._img_widget.setFixedSize(self.ui.imageFrame.size())
                self._gray_frame = cv2.imread(self._gray_image_file, 0)
                self._img_widget.set_frame(self._gray_frame)
                self._color_img = None
        elif btn == 'process':
            if self._gray_frame is not None:
                self.ui.leftFrame.setDisabled(True)
                self.ui.rightMenu.setDisabled(True)
                self.ui.progressBar.setValue(0)
                self._p_timer.start(300)
                threading.Thread(target=self._colorize_frame).start()
            else:
                show_message(msg='Please select a gray image!', msg_type="Warning")
        elif btn == 'save':
            if self._color_frame is not None:
                file_name, _ = QFileDialog.getSaveFileName(
                    self, caption="Save Colorized Image to F:xile",
                    filter=f"Images (*.{get_config().get('save_format', 'jpg')})")
                if file_name:
                    cv2.imwrite(file_name, self._color_frame)
                    logger.info(f"Saved colorized result to a file - {file_name}")
        elif btn == 'clear':
            self._gray_frame = None
            self._color_frame = None
            if self._img_widget is not None:
                self._img_widget.deleteLater()
                self._img_widget = None
                self.ui.imageLayout.addWidget(self._select_btn)

    def _update_progress_bar(self):
        self.ui.progressBar.setValue(self.ui.progressBar.value() + 1)

    def _colorize_frame(self):
        _color_frame = self._deoldifier.process_frame(self._gray_frame, render_factor=self._render_factor)
        logger.info(f"Colorized gray image - {self._gray_image_file} => {_color_frame.shape}")
        getattr(self, 'finished').emit(_color_frame)

    def _on_finished(self, color_frame):
        self._color_frame = color_frame
        self.ui.leftFrame.setDisabled(False)
        self.ui.rightMenu.setDisabled(False)
        self._p_timer.stop()
        self.ui.progressBar.setValue(0)
        self._img_widget.set_frame(self._color_frame)


if __name__ == '__main__':

    logger.info('========== Starting ColorizeLife Application ==========')

    sys._excepthook = sys.excepthook


    def exception_hook(exctype, value, tb):
        logger.error("=========== Crashed!", exc_info=(exctype, value, tb))
        getattr(sys, "_excepthook")(exctype, value, tb)
        sys.exit(1)


    sys.excepthook = exception_hook

    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon('colorize_life.ico'))
    ex = ColorizeLifeApp()
    ex.show()
    sys.exit(app.exec_())
