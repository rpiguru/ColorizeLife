import json
import os
import platform
import subprocess
import signal
import sys
import threading


_cur_dir = os.path.dirname(os.path.realpath(__file__))
_par_dir = os.path.join(_cur_dir, os.path.pardir)
if _par_dir not in sys.path:
    sys.path.append(_par_dir)


from utils.logger import logger
from settings import CONFIG_FILE


default_conf = {'render_factor': 25 if platform.system() == 'Windows' else 20, 'save_format': 'jpg'}


if not os.path.exists(CONFIG_FILE):
    logger.warning('No JSON Config File Found! Recovering the default one...')
    with open(CONFIG_FILE, 'w') as jp:
        json.dump(default_conf, jp, indent=2)


def number_to_ordinal(n):
    """
    Convert number to ordinal number string
    """
    return "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4])


def check_running_proc(proc_name):
    """
    Check if a process is running or not
    :param proc_name:
    :return:
    """
    try:
        cmd = "ps -aef | grep -i '%s' | grep -v 'grep' | awk '{ print $3 }'" % proc_name
        lines = os.popen(cmd).read().strip().splitlines()
        return [int(pid) for pid in lines]
    except Exception as e:
        print('Failed to get status of the process({}) - {}'.format(proc_name, e))
    return False


def kill_process_by_name(proc_name, args="", use_sudo=True, sig=None):
    sig = sig if sig is not None else signal.SIGKILL
    p = subprocess.Popen(['ps', '-efww'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    for line in out.decode().splitlines():
        if proc_name in line and (not args or args in line):
            pid = int(line.split()[1])
            logger.info(f'Found PID({pid}) of `{proc_name}`, killing with {sig}...')
            if use_sudo:
                os.system(f'sudo kill -{sig} {pid}')
            else:
                os.kill(pid, sig)


def update_dict_recursively(dest, updated):
    """
    Update dictionary recursively.
    :param dest: Destination dict.
    :type dest: dict
    :param updated: Updated dict to be applied.
    :type updated: dict
    :return:
    """
    for k, v in updated.items():
        if isinstance(dest, dict):
            if isinstance(v, dict):
                r = update_dict_recursively(dest.get(k, {}), v)
                dest[k] = r
            else:
                dest[k] = updated[k]
        else:
            dest = {k: updated[k]}
    return dest


lock = threading.Lock()


def update_config_file(data):
    old_data = update_dict_recursively(dest=get_config(), updated=data)
    with lock:
        with open(CONFIG_FILE, 'w') as jp:
            json.dump(old_data, jp, indent=2)


def get_config():
    global lock
    with lock:
        try:
            conf = json.loads(open(CONFIG_FILE).read())
            return conf
        except Exception as e:
            logger.error(f"Failed to read config file ({e})")
