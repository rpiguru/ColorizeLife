import http.client
import json
from urllib.parse import urlencode

from utils.logger import logger


BASE_URL = "colorizelife.com:443"
HEADERS = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}


def registration(sl):
    params = urlencode({
        'edd_action': 'activate_license',
        # Valid actions are activate_license, deactivate_license, get_version, check_license
        'license': sl,
        'item_name': 'Colorize Life for Windows',
        # 'url' : 'domain.com' # If you have disabled URL chaecking in the settings, you do not need this
    })
    try:
        conn = http.client.HTTPSConnection(BASE_URL)  # You can use 80 (http) or 443 (https)
        conn.request("POST", "/", params, HEADERS)  # /edd-sl is the software licensing api endpoint
        response = conn.getresponse()
        if response.status == 200:
            result = json.loads(response.read().decode('utf-8'))
            return result
    except Exception as e:
        logger.error(f"Failed to activate license - {e}")


def get_license_status(sl):
    """
    :param sl:
    :return:
        Success:
            {'success': True, 'license': 'inactive', 'item_id': False, 'item_name': 'Colorize Life for Windows',
            'checksum': '32ea9c554ae8eb9d3d0f973a0388caa8', 'expires': '2021-12-30 23:59:59', 'payment_id': 70,
            'customer_name': 'Robert Evans', 'customer_email': 'admin@colorizelife.com', 'license_limit': 1,
            'site_count': 0, 'activations_left': 1, 'price_id': False}
        Fail:
            {'success': False, 'license': 'invalid', 'item_id': False, 'item_name': 'Colorize Life for Windows',
            'checksum': '9e577823852bc34a250ef1118817c680'}
    """
    params = urlencode({
        'edd_action': 'check_license',
        # Valid actions are activate_license, deactivate_license, get_version, check_license
        'license': sl,
        'item_name': 'Colorize Life for Windows',
        # 'url' : 'domain.com' # If you have disabled URL chaecking in the settings, you do not need this
    })
    try:
        conn = http.client.HTTPSConnection(BASE_URL)  # You can use 80 (http) or 443 (https)
        conn.request("POST", "/", params, HEADERS)  # /edd-sl is the software licensing api endpoint
        response = conn.getresponse()
        if response.status == 200:
            return json.loads(response.read().decode('utf-8'))
    except Exception as e:
        logger.error(f"Failed to check license - {e}")


def check_license(sl):
    status = get_license_status(sl)
    if status is not None:
        if status.get('license') == 'valid':
            return status
        elif status.get('license') == 'inactive':
            logger.info(f"Not activated yet, registering now...")
            return registration(sl)


if __name__ == '__main__':

    print(registration('c9b74bf5bfb17b26995429f123b4d17a'))

    print(get_license_status('c9b74bf5bfb17b26995429f123b4d17a'))
